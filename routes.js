import { StackNavigator } from 'react-navigation';
import { createStackNavigator, createAppContainer } from 'react-navigation';

//import Home from './App/Home.react';
// components
import MainPage from "./components/layouts/main-page";
import DietPage from "./components/layouts/diet-page"
import ProfilePage from "./components/layouts/profile-page"

const AppNavigator = StackNavigator({
    main: { screen: MainPage },
    diet: { screen: DietPage },
    profile: { screen: ProfilePage },
   
}, {
    headerMode: 'none',
});

export default createStackNavigator(AppNavigator);