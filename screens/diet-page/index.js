import React from 'react';
import {ActivityIndicator, StyleSheet} from 'react-native';
import {Screen, View, Text, ListView, Icon, Button} from '@shoutem/ui';
import BottomNavigation from '../../components/bottom-navigation';
import mealsRequest from "../../network/mealsRequest";
import {connect} from "react-redux";
import dietSetRequest from "../../network/dietSetRequest";
import userDietRequest from "../../network/userDietRequest";
import showSnackBar from "../../components/showSnackbar";
import getUserInfoRequest from "../../network/getUserInfoRequest";
import NativeToastAndroid from "react-native/Libraries/Components/ToastAndroid/NativeToastAndroid";
const styles = StyleSheet.create({
  section__fs: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
    zIndex: 1,
    // backgroundColor: 'green',
  },
  section__fs1: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    // marginBottom: 15,
    paddingBottom: 50,
    // paddingVertical: 10,
    paddingHorizontal: 10,
    zIndex: 1,
    // backgroundColor: 'green',
  },
  view: {flex: 1},
  caloriesText: {
    fontSize: 16,
  },
  productRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 15,
    marginVertical: 7,
  },
  caloriesTextHolder: {
    paddingTop: 5,
    display: 'flex',
  },
  section__title: {
    fontSize: 18,
    marginTop: 20,
    marginBottom: 10,
  },
  key: {
    fontSize: 14,
    marginRight: 8,
  },
  value: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FF5722',
  },
  value1: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'red',
    width: '100%',
    textAlign: 'center',
  },
  col: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  between__text: {
    marginLeft: 20,
    marginRight: 20,
    fontSize: 34,
    fontWeight: 'bold',
  },
  fab1__icon: {
    color: '#fff',
  },
  fab1: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#FF5722',
    color: '#fff',
  },
  container: {
    flex:1,
    backgroundColor: '#f2f2f2',
    paddingTop: 10,
  },
  section: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    flexWrap: 'wrap',
  },
  section__sb: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    flexWrap: 'wrap',
    paddingHorizontal: 20,
    paddingVertical: 8,
  },
  section__se: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    flexWrap: 'wrap',
    paddingVertical: 10,
  },
  section__se1: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 20,
    flexWrap: 'wrap',
    paddingVertical: 10,
  },
  section11: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
    zIndex: 1,
  },
  section111: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
    zIndex: 2,
  },
  section1: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  input: {
    flex: 1,
    flexGrow: 1,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
    color: 'white',
    width: 400,
  },
  containerSelect: {
    flexDirection: 'column',
    marginTop: 20,
    marginBottom: 20,
  },
  modalContainer: {
    width: 300,
    height: 400,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 10,
  },
  topNav: {},
  inputRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: '80%',
  },
  productForm: {
    flex: 0.2,
    // flexDirection: 'column',
    // marginTop: 10,
    alignItems: 'center',
    // backgroundColor: 'gray'
    // justifyContent: 'space-around's
  },
  autocompleteContainer: {
    paddingTop: 0,
    zIndex: 1,
    // height: '15%',
    width: '100%',
    paddingHorizontal: 8,
    // display: 'flex',
    // flex: .05,
    flexDirection: 'row',
    backgroundColor: 'green',
    // marginLeft: 0,
    // marginRight: 0,
    // justifyContent: 'center',

    // backgroundColor: 'gray'
  },
  inputAutocomplete: {
    // display: 'flex',
    width: '100%',
    // flex: 1,
    // flexDirection: 'row',
    // alignItems: 'center',
    // width: '100%',
  },
  inputContainer: {
    display: 'flex',
    flexShrink: 0,
    flexGrow: 0,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#c7c6c1',
    paddingVertical: 13,
    paddingLeft: 12,
    paddingRight: '5%',
    width: '100%',
    backgroundColor: 'gray',

    justifyContent: 'flex-start',
  },
  plus: {
    position: 'absolute',
    left: 15,
    top: 10,
    width: 20,
    height: 20,
  },
  addMealBtn: {
    width: 70,
    height: 40,
    color: 'red',
  },
  selectContainer: {
    flex: 0.4,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    height: 20,
    alignItems: 'center',
  },
  select: {
    flex: 1,
    marginHorizontal: 4,
    width: '60%',
  },
  root: {backgroundColor: '#fff'},
  wrapper: {flex: 1, paddingBottom: 71},
});

class DietPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      dietList: [],
      token: '',
      userDiet: null,
      userInfo: null,
      loading: true
    };
    this.host = 'http://tech.splinex-team.com:6667';
    this.diet_list_url = `${this.host}/api/v1/user/diet/get-all`;
    //`${this.product_search_url}/?name="${this.state.autocompleteValue}"`
  }

  componentWillMount() {
    const token = `Bearer_${this.props.token}`;
    this.setState(
      {
        token: token,
      },
      async () => {
        this.getDietList()
      },
    );
  }

  getDietList() {
    let url =`${this.diet_list_url}`;
    let headers = {
      'Content-Type': 'application/json',
      Authorization: this.state.token,
    };
    fetch(url, {
      method: 'GET',
      headers: headers,
      mode: 'no-cors',
      cache: 'no-cache',
      redirect: 'manual',
    })
      .then(response => response.json())
      .then(resp => {
        console.log("RESPONSE");
        console.log(resp);

        this.setState({dietList:resp}, () => {
          this.getUserDiet().then(r => {
            this.getUserInfo()
          });
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  getUserDiet = async () => {
    let response = await userDietRequest(this.state.token);
    console.log('user diet resp');
    console.log(response);
    if (response.error === null) {
      console.log(response.result);
      this.setState({loading: false, userDiet: response.result.id});

      return true;
    } else {
      if (response.result === null) {
        console.log('IFFFFF');
        this.setState({loading: false});
      }
    }

    // this.setState({loading: false,})
  };

  getUserInfo = async () => {
    let response = await getUserInfoRequest(this.state.token);
    console.log('user info request');
    console.log(response);
    if (response.error === null) {
      this.setState({
        userInfo: response.result,
      }, () => {
        let dietList = this.state.dietList;
        if (this.state.userInfo.goalWeight < 60) {
          dietList[0].isrecommend = true;
          dietList[1].isrecommend = false;
        } else {
          dietList[0].isrecommend = false;
          dietList[1].isrecommend = true;
        }
        console.log('DIET LIST')
        console.log(dietList);
        this.setState({dietList: dietList, loading: false});
      });
    }
  };

  selectDiet = async (id) => {
    console.log(id);
    let response = await dietSetRequest(this.state.token, id);
    console.log(response);
    if (response.error === null) {
      console.log('ok');
      NativeToastAndroid.show('Диета выбрана', NativeToastAndroid.SHORT);
      //FIXME: this shit not work!!!!!
      this.setState({loading: true});
      this.getDietList();
      showSnackBar('Диета выбрана', 500, 'OK', () => {});
    }
  };

  renderDiet = (diet) => {
    console.log('diet');
    console.log(diet);
    const id = diet.id;

    return (
        <View
          style={{borderBottomWidth: 2, borderBottomColor: '#FF5722', paddingVertical: 20, paddingHorizontal: 10}}>
          {diet.isrecommend &&
          <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 5}}>
            <Text style={styles.value1}> Рекомендованная диета </Text>
          </View>
          }
          <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 5}}>
            <Text style={{marginRight: 10}}>Название диеты:</Text><Text style={styles.value}>{diet.name}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 5}}>
            <Text style={{marginRight: 10}}>Сложность диеты:</Text><Text style={styles.value}>{diet.difficulty} / 10</Text>
          </View>

          <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 5}}>
            <Text style={{marginRight: 10}}>Белки:</Text>
            <Text style={styles.value}>{diet.protein}%</Text>
            <Text style={{marginRight: 10}}> </Text>
            <Text style={{marginRight: 10}}>Жиры:</Text>
            <Text style={styles.value}>{diet.fat}%</Text>
            <Text style={{marginRight: 10}}> </Text>
            <Text style={{marginRight: 10}}>Углеводы:</Text>
            <Text style={styles.value}>{diet.carb}%</Text>
          </View>
          <View style={{ marginBottom: 5, marginTop: 5}}>
            <Text style={styles.value}>Описание:</Text>
            { diet.description ? (<Text>{diet.description}</Text>):(<Text style={{marginVertical: 10, fontSize:16, fontWeight: 'bold'}}>Описание отсутствует.</Text>)}
          </View>
          <View>
            {this.state.userDiet === id
                ? (<Text>Диета выбрана ✔✔✔</Text>)
                : (<Button onPress={()=>{this.selectDiet(id)}}><Text>Выбрать</Text></Button>)
            }
          </View>
        </View>
    );
  };

  renderDietList = () =>{
    if (this.state.dietList.length === 0) {
      return (
        <Text style={{marginVertical: 10, fontSize:20, fontWeight: 'bold'}}>Диеты отсутствуют</Text>
      )
    } else {
      return (
        <ListView
            style={{marginBottom:200, paddingBottom: 100}}
            data={this.state.dietList}
            renderRow={this.renderDiet}
        />
      )
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <Screen style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
          <View>
            <ActivityIndicator size={50}/>
          </View>
        </Screen>
      );
    }
    console.log('DSFDSFDFSDFS');
    console.log(this.state.dietList);
    if (this.state.dietList[0].isrecommend !== undefined) {
      return (
          <Screen style={styles.root}>
            <View style={styles.wrapper}>
              <Button
                  style={{
                    position: 'absolute',
                    top:5,
                    left:-10,
                    color: 'black',
                    fontSize: 40
                  }}
                  styleName="clear"
                  onPress={()=>{this.props.history.goBack()}}
              >
                <Icon name="left-arrow" style={{color: 'black', fontSize: 40}} />
              </Button>
              <View style={{flex:1, marginTop: 50}}>
                <View style={styles.section1}>
                  <Text style={{marginVertical: 10, fontSize:20, fontWeight: 'bold', color: 'black'}}>Выбор диеты</Text>
                </View>
                <View style={styles.section__fs1}>
                  {this.renderDietList()}
                </View>
              </View>
            </View>
            <BottomNavigation />
          </Screen>
      );
    }
    return (
        <Screen style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
          <View>
            <ActivityIndicator size={50}/>
          </View>
        </Screen>
    );

  }
}


const mapStateToProps = state => ({
  token: state.authentication.token,
});

export default connect(mapStateToProps)(DietPage);
