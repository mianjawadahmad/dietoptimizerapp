import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Screen, View, TextInput, Button, Text} from '@shoutem/ui';
import {Link} from 'react-router-native';
import showSnackBar from '../../../components/showSnackbar';
import registrationRequest from "../../../network/registrationRequest";
const styles = StyleSheet.create({
  screen: {
    justifyContent: 'center',
  },
  wrapper: {
    width: '100%',
  },
  input: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
  },
  button: {
    backgroundColor: '#FF5722',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
  },
  buttonCheckbox: {
    backgroundColor: '#6f2914',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
  },
  buttonCheckboxSelected: {
    backgroundColor: '#FF5722',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
  },
  buttonText: {color: '#fff'},
  logo: {
    width: 140,
    height: 148,
    resizeMode: 'stretch',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 20,
  },
  text: {
    marginTop: 5,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  inputSmall: {
    // marginLeft: 10,
    // marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
    width: '45%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
    // marginLeft: 10,
    // marginRight: 10,

  }
});

export default class RegistrationView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      password_repeat: '',
      email: '',
      firstName: '',
      lastName: '',
      height: '',
      weight: '',
      sex: 0,
      age: '',
    };
  }

  validateEmail = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  registerClick = async () => {
    console.log('click');
    console.log(this.state);

    let {username, password, password_repeat, email, firstName, lastName, weight, height, age, sex} = this.state;
    email = email.toLowerCase();

    console.log(this.validateEmail(email));

    if (!username || !password || !password_repeat || !email || !firstName || !lastName || !age || !weight || !height) {
      showSnackBar('Заполните все поля', 'SHORT', 'ОК', () => {});
      return false;
    }
    if (password !== password_repeat){
      showSnackBar('Пароли не совпадают', 'SHORT', 'ОК', () => {});
      return false;
    }
    if (!this.validateEmail(email)) {
      showSnackBar('Введите корректный email', 'SHORT', 'ОК', () => {});
      return false;
    }
    if (password.length < 6) {
      showSnackBar('Пароль слишком простой', 'SHORT', 'ОК', () => {});
      return false;
    }

    let response = await registrationRequest(username, password, email, firstName, lastName, age, weight, height, sex);
    console.log(response);
    if (response.result !== null) {
      showSnackBar('Регистрация успешна', 'SHORT', 'ОК', () => {});
      console.log(response.result);
      this.props.history.push('/');
    } else {
      if (response.error) {
        if (response.error.errors) {
          console.log(response);
          if (response.errors && response.error.errors.includes('EMAIL_EXISTS')) {
            showSnackBar('Заданный почтовый адрес уже занят', 'SHORT', 'ОК', () => {});
          } else {
            if (response.error.errors) {
              showSnackBar(`Ошибка регистрации [${response.error.errors.join(', ')}]`, 'SHORT', 'ОК', () => {});
            }
            if (response.error && response.error.errors === undefined) {
              showSnackBar(`Ошибка регистрации [${response.error}]`, 'SHORT', 'ОК', () => {});
            }
          }
        }
      }

    }
  };

  render() {
    return (
      <Screen style={styles.screen}>
        <View style={styles.wrapper}>
          <View style={styles.row}>
            <TextInput onChangeText={(val)=>{
              this.setState({
                username: val,
              });
            }} style={styles.inputSmall} placeholder={'Логин'} />
            <TextInput onChangeText={(val)=>{
              this.setState({
                email: val,
              });
            }} style={styles.inputSmall} placeholder={'Почта'} />
          </View>
          <View style={styles.row}>
            <TextInput onChangeText={(val)=>{
              this.setState({
                firstName: val,
              });
            }} style={styles.inputSmall} placeholder={'Имя'} />
            <TextInput onChangeText={(val)=>{
              this.setState({
                lastName: val,
              });
            }} style={styles.inputSmall} placeholder={'Фамилия'} />
            </View>
          <View style={styles.row}>
            <TextInput
                onChangeText={(val) => {
                  // console.log(val.length)
                  let value = val.length ? Math.abs(parseInt(val)) : 0;
                  // console.log(value)
                  this.setState({
                    height: value,
                  });
                }}
                keyboardType={'numeric'}
                style={styles.inputSmall} placeholder={'Рост, см'}
            />
            <TextInput
                onChangeText={(val) => {
                  let value = val.length ? Math.abs(parseInt(val)) : 0;
                  this.setState({
                    weight: value,
                  });
                }}
                keyboardType={'numeric'}
                style={styles.inputSmall} placeholder={'Вес, кг'}
            />
          </View>
          <View style={styles.row}>
            <TextInput
                onChangeText={(val) => {
                  let value = val.length ? Math.abs(parseInt(val)) : 0;
                  this.setState({
                    age: value,
                  });
                }}
                keyboardType={'numeric'}
                style={styles.inputSmall} placeholder={'Вораст'}
            />
            <View style={{width: '45%', display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
              <Button onPress={() => {
                this.setState({sex: 0})
              }} style={this.state.sex === 0 ? styles.buttonCheckboxSelected : styles.buttonCheckbox}>
                <Text style={styles.buttonText}>Муж</Text>
              </Button>
              <Button onPress={() => {
                this.setState({sex: 1})
              }} style={this.state.sex === 1 ? styles.buttonCheckboxSelected : styles.buttonCheckbox}>
                <Text style={styles.buttonText}>Жен</Text>
              </Button>
            </View>
          </View>
          <TextInput
            style={styles.input}
            placeholder={'Пароль'}
            secureTextEntry
            onChangeText={(val)=>{
              this.setState({
                password: val,
              });
            }}
          />
          <TextInput
            style={styles.input}
            placeholder={'Пароль повторно'}
            secureTextEntry
            onChangeText={(val)=>{
              this.setState({
                password_repeat: val,
              });
            }}
          />
          <Button onPress={this.registerClick} style={styles.button}>
            <Text style={styles.buttonText}>Регистрация</Text>
          </Button>
          <Link style={styles.text} to="/">
            <Text>Вход</Text>
          </Link>
        </View>
      </Screen>
    );
  }
}
