import React from 'react';
import {StyleSheet} from 'react-native';
import {Screen, View, TextInput, Text, Image} from '@shoutem/ui';
import {Link} from 'react-router-native';
import LoadingButton from './LoadingButton';

const styles = StyleSheet.create({
  screen: {
    justifyContent: 'center',
  },
  wrapper: {
    width: '100%',
    alignItems: 'center',
  },
  input: {
    width: 340,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
  },
  logo: {
    width: 140,
    height: 148,
    resizeMode: 'stretch',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 20,
  },

  link: {
    marginTop: 5,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});

const LoginView = props => {
  const {
    onLoginInputChange,
    onPasswordInputChange,
    loginValue,
    passwordValue,
    onPress,
    isLoading,
  } = props;
  return (
    <Screen style={styles.screen}>
      <View style={styles.wrapper}>
        <Image style={styles.logo} source={require('./logo.png')} />
        <TextInput
          style={styles.input}
          placeholder={'Логин'}
          value={loginValue}
          onChangeText={onLoginInputChange}
          editable={!isLoading}
        />
        <TextInput
          style={styles.input}
          placeholder={'Пароль'}
          secureTextEntry
          value={passwordValue}
          onChangeText={onPasswordInputChange}
          editable={!isLoading}
        />
        <LoadingButton
          buttonText="Войти"
          onPress={onPress}
          isLoading={isLoading}
        />
        <Link style={styles.link} to="/registration">
          <Text>Регистрация</Text>
        </Link>
      </View>
    </Screen>
  );
};

export default LoginView;
