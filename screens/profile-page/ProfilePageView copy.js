import React from 'react';
import {StyleSheet, ActivityIndicator, ToastAndroid} from 'react-native';
import {
  Screen,
  View,
  Text,
  Subtitle,
  Image,
  Divider,
  Button,
  TextInput, DropDownMenu, Title, NavigationBar,
} from '@shoutem/ui';
import BottomNavigation from '../../components/bottom-navigation';
import Graphics from '../../components/graphics';
import PopupDialog from '../../components/popup-dialog';
import {withRouter} from 'react-router-native';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
  view: {flex: 4},
  header: {
    flexDirection: 'row',
    marginTop: 20,
    marginLeft: 10,
  },
  profileImage: {
    width: 100,
    height: 100,
    marginBottom: 20,
    marginRight: 10,
    borderWidth: 1,
    borderColor: '#A6A6A6',
  },
  devider: {
    borderColor: '#A6A6A6',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    width: 130,
    height: 40,
    marginTop: 10,
  },
  buttonDialog: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    height: 40,
    marginTop: 10,
  },
  buttonDelete: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    height: 40,
    marginTop: 10,
    marginLeft: 70,
    marginRight: 70,
  },
  buttonDelete1: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    height: 40,
    marginTop: 10,

    marginRight: 70,
  },
  buttonText: {color: '#fff'},
  icon: {
    color: '#A6A6A6',
    marginLeft: 'auto',
    marginRight: 40,
    fontSize: 20,
  },
});
const ProfilePageView = props => {
  const {
    name,
    weight,
    goalWeight,
    graphData,
    isVisibleDiaolog,
    setVisibleDiaolog,
    dialogInputValue,
    setDialogInputValue,
    onDialogButtonPress,
    onDeleteButtonPress,
    history,
  } = props;
  let {
    id,
    username,
    firstName,
    lastName,
    email,
    age,
  } = userInfo ? userInfo : {id:0,username:'',firstName:'',lastName:'',email:'',age:0};
  let goalWeight1 =  userInfo ? userInfo.goalWeight : 0;

  username = username.toString();
  goalWeight1 = goalWeight1 === null ? '' : goalWeight1.toString();
  firstName = firstName === null ? '' : firstName.toString();
  lastName = lastName === null ? '' : lastName.toString();
  email = email === null ? '' : email.toString();
  age = age === null ? '' : age.toString();
  let height = 180;
  let bigness = '0';
  let weight1 = weight.toString();

  const dialogContent = (
    <>
      <TextInput
        style={styles.input}
        placeholder={'Новый вес'}
        value={dialogInputValue}
        onChangeText={(val)=>{
          // console.log(val)
          let value = val.length !== 0 ? val : 0;
          value = parseInt(val) > 0 ? ''+parseInt(val) : ''+Math.abs(val);
          console.log(value);
          setDialogInputValue(value);
        }}
        keyboardType={'numeric'}
      />
      <Button
        style={styles.buttonDialog}
        disabled={!dialogInputValue}
        onPress={() => {
          onDialogButtonPress();
          setVisibleDiaolog(false);
        }}>
        <Text style={styles.buttonText}>ОК</Text>
      </Button>
    </>
  );
  if (loading){
    return (
        <Screen style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
          <View>
            <ActivityIndicator size={50}/>
          </View>
        </Screen>
    );
  }
  let opts = ['o1', 'o2'];
  let selO = 'o1';
  let gender = 'М';
  return (
    <Screen>
      <View style={styles.view}>
        <View style={styles.header}>
          <Image
            styleName="medium-avatar"
            style={styles.profileImage}
            source={require('./profileImage.jpg')}
          />
          <View>
            <Subtitle>{name}</Subtitle>
            <Text>Ваш текущий вес: {weight}кг</Text>
            <Text>Цель: {goalWeight}кг</Text>
            <View style={{flexDirection: 'row', justifyContent:'flex-start', marginBottom: 20}}>
              <Button
                style={styles.button}
                onPress={() => setVisibleDiaolog(true)}>
                <Text style={styles.buttonText}>Задать вес</Text>
              </Button>
              <Button
                  style={{display: 'none'}}
                // style={styles.buttonDelete1}
                onPress={() => onDeleteButtonPress()}>
                <Text style={styles.buttonText}>Удалить последний вес</Text>
              </Button>
            </View>

            <PopupDialog
              dialogContent={dialogContent}
              dialogTitle="Задать вес"
              isVisibleDiaolog={isVisibleDiaolog}
              setVisibleDiaolog={setVisibleDiaolog}
              setDialogInputValue={setDialogInputValue}
            />
          </View>
          <Icon
            name="ios-settings"
            style={styles.icon}
            onPress={() => {
              history.push('/profile-settings');
            }}
          />
        </View>
        <Divider styleName="line" style={styles.devider} />
        <Graphics graphData={graphData} />

      </View>
      <View style={{flexDirection: 'column', alignItems: 'center', marginBottom: 30, flex:4, backgroundColor: '#f2f2f2', borderTopColor:'black',borderTopWidth:1,paddingTop:10}}>
        <Text>Имя:</Text>
        <TextInput onChangeText={(text)=>{console.log(text); firstName=text}}
                   style={{width:'95%', marginBottom: 8}}
                   placeholder={firstName || ''}
        />
        <Text>Вес, кг (цель):</Text>
        <TextInput keyboardType={'numeric'}
                   onChangeText={(text)=>{
                     if (parseInt(text) < 0) {
                       text = '0';
                     }
                     goalWeight1 = parseInt(text)}
                   }
                   style={{width:'95%', marginBottom: 8}}
                   placeholder={goalWeight1 || ''}
        />
        <Text>Рост, см:</Text>
        <TextInput keyboardType={'numeric'}
                   onChangeText={(val)=>{
                     if (parseInt(val) < 0) {
                       val = '0'
                     }
                     if (parseInt(val) > 1000) {
                       val = '1000'
                     }
                     height=parseInt(val)}
                   }
                   style={{width:'95%', marginBottom: 8}}
                   placeholder={height.toString() || ''}
        />
        <Text>Возраст:</Text>
        <TextInput keyboardType={'numeric'}
                   onChangeText={(text)=>{
                     console.log(text);
                     if (parseInt(text) < 0) {
                       text = '0'
                     }
                     age=parseInt(text)}
                   }
                   style={{width:'95%', marginBottom: 4}}
                   style={{width:'95%', marginBottom: 8}}
                   placeholder={age || ''}

        />
        <View style={{display: 'none'}}>
          <Text>Пол:</Text>
          <TextInput keyboardType={'numeric'}
                     onChangeText={(text)=>{console.log(text); gender=parseInt(text)}} style={{width:'95%', marginBottom: 4}}/>
        </View>

        <Button
          style={styles.buttonDelete}
          onPress={() => {
            console.log(height);
            const data = {
              id,
              goalWeight: parseInt(goalWeight1),
              firstName,
              lastName,
              age: parseInt(age)
            };

            if (data.goalWeight === 0 || data.age === 0) {
              console.log('ERROR');
              NativeToastAndroid.show('Введите корректные данные', NativeToastAndroid.SHORT);
              showSnackBar('Введите корректные данные', 500, 'OK', ()=> {});
              return false;
            }
            console.log(data);
            let headers = {
              'Content-Type': 'application/json',
              Authorization:  'Bearer_' + token,
            };
            fetch('http://tech.splinex-team.com:6667' + '/api/v1/user/update-info', {
              method: 'POST',
              mode: 'no-cors',
              cache: 'no-cache',
              redirect: 'manual',
              headers: headers,
              body: JSON.stringify(data),
            })
              .then(response => response.json())
              .then(resp => {
                console.log(resp);
                showSnackBar('Изменения сохранены', 'SHORT', 'ОК', () => {});
              })
              .catch(error => {
                console.log(error);
                showSnackBar('Введите корректные данные', 'SHORT', 'ОК', () => {});
              });
          }}>
          <Text style={styles.buttonText}>Сохранить</Text>
        </Button>
      </View>
      <BottomNavigation />
    </Screen>
  );
};

export default withRouter(ProfilePageView);
