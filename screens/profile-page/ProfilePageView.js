import React from 'react';
import {ActivityIndicator, StyleSheet} from 'react-native';
import {
  Screen,
  View,
  Text,
  Subtitle,
  Image,
  Divider,
  Button,
  TextInput,
} from '@shoutem/ui';
import BottomNavigation from '../../components/bottom-navigation';
import Graphics from '../../components/graphics';
import PopupDialog from '../../components/popup-dialog';
import {withRouter} from 'react-router-native';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
  view: {flex: 1},
  header: {
    flexDirection: 'row',
    marginTop: 50,
    marginLeft: 10,
  },
  profileImage: {
    width: 100,
    height: 100,
    marginBottom: 20,
    marginRight: 10,
    borderWidth: 1,
    borderColor: '#A6A6A6',
  },
  devider: {
    borderColor: '#A6A6A6',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    width: 130,
    height: 40,
    marginTop: 10,
  },
  buttonDialog: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    height: 40,
    marginTop: 10,
  },
  buttonDelete: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    height: 40,
    marginTop: 10,
    marginLeft: 70,
    marginRight: 70,
  },
  settingsBtn: {
    backgroundColor: '#FF5722',
    borderRadius: 40,
    height: 40,
    width: 40,
    marginLeft: 'auto',
    marginRight: 15,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {color: '#fff'},
  icon: {
    color: 'white',
    // marginLeft: 'auto',
    // marginRight: 15,
    fontSize: 30,
  },
});
const ProfilePageView = props => {
  const {
    name,
    weight,
    goalWeight,
    graphData,
    isVisibleDiaolog,
    setVisibleDiaolog,
    dialogInputValue,
    setDialogInputValue,
    onDialogButtonPress,
    onDeleteButtonPress,
    history,
    isLoaded,
  } = props;
  const dialogContent = (
    <>
      <TextInput
        style={styles.input}
        placeholder={'Новый вес'}
        value={dialogInputValue}
        onChangeText={setDialogInputValue}
        keyboardType={'numeric'}
      />
      <Button
        style={styles.buttonDialog}
        disabled={!dialogInputValue}
        onPress={() => {
          onDialogButtonPress();
          setVisibleDiaolog(false);
        }}>
        <Text style={styles.buttonText}>ОК</Text>
      </Button>
    </>
  );
  if (!isLoaded) {
    return (
        <Screen>
          <View style={styles.view}>
            <ActivityIndicator style={{marginTop: 350, color: 'white'}} size="large" />
          </View>
        </Screen>
    );
  }
  return (
    <Screen>
      <View style={styles.view}>
        <View style={styles.header}>
          <Image
            styleName="medium-avatar"
            style={styles.profileImage}
            source={require('./profileImage.jpg')}
          />
          <View style={{marginBottom:10}}>
            <Subtitle>{name}</Subtitle>
            <Text>Ваш текущий вес: {weight}кг</Text>
            <Text>Цель: {goalWeight}кг</Text>
            <Button
              style={styles.button}
              onPress={() => setVisibleDiaolog(true)}>
              <Text style={styles.buttonText}>Задать вес</Text>
            </Button>
            <PopupDialog
              dialogContent={dialogContent}
              dialogTitle="Задать вес"
              isVisibleDiaolog={isVisibleDiaolog}
              setVisibleDiaolog={setVisibleDiaolog}
              setDialogInputValue={setDialogInputValue}
            />
          </View>
          <Button style={styles.settingsBtn}>
            <Icon
                name="ios-settings"
                style={styles.icon}
                onPress={() => {
                  history.push('/profile-settings');
                }}
            />
          </Button>
        </View>
        <Divider styleName="line" style={styles.devider} />
        <Graphics graphData={graphData} />
        <Button
          style={styles.buttonDelete}
          onPress={() => onDeleteButtonPress()}>
          <Text style={styles.buttonText}>Удалить последний вес</Text>
        </Button>
      </View>
      <BottomNavigation />
    </Screen>
  );
};

export default withRouter(ProfilePageView);
