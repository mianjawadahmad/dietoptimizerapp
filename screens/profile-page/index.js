import React, {useState, useEffect} from 'react';
import ProfilePageView from './ProfilePageView';
import getUserInfoRequest from '../../network/getUserInfoRequest';
import weightRequest from '../../network/weightRequest';

import {useSelector} from 'react-redux';
import showSnackBar from '../../components/showSnackbar';

const ProfilePage = () => {
  const [userInfo, setUserInfo] = useState(null);
  const [weightList, setWeightList] = useState(null);
  const [dialogInputValue, setDialogInputValue] = useState(null);
  const [isVisibleDiaolog, setVisibleDiaolog] = useState(false);
  const [isLoaded, setLoaded] = useState(false);
  const token = useSelector(state => state.authentication.token);
  const graphData = {
    labels:
      weightList && weightList.length
        ? weightList.map((item, index) => index)
        : [0, 0, 0, 0, 0, 0],
    datasets: [
      {
        data:
          weightList && weightList.length
            ? weightList.map(item => item.value)
            : [0, 0, 0, 0, 0, 0],
        color: (opacity = 1) => `rgba(255, 87, 34, ${opacity})`, // optional
        strokeWidth: 1, // optional
      },
    ],
  };
  const {firstName, goalWeight} = userInfo
    ? userInfo
    : {
        firstName: 'firstName',
        goalWeight: 0,
      };
  const currentWeight =
    weightList && weightList.length
      ? weightList[weightList.length - 1].value
      : 0;

  const lastWeightNumber =
    weightList && weightList.length ? weightList[weightList.length - 1].id : 0;

  const makeUserInfoRequest = async () => {
    const {result, error} = await getUserInfoRequest(token);
    if (!error) {
      setUserInfo(result);
      setLoaded(true);
    } else {
      showSnackBar(error.message, 'SHORT', 'ОК', () => {});
    }
  };

  const makeWeightRequest = async () => {
    const {result, error} = await weightRequest('GET', 'get-all', token);
    if (!error) {
      setWeightList(result);
    } else {
      showSnackBar(error.message, 'SHORT', 'ОК', () => {});
    }
  };

  const addWeightRequest = async () => {
    await weightRequest('POST', 'add', token, dialogInputValue);
    makeWeightRequest();
  };

  const deleteWeightRequest = async () => {
    await weightRequest(
      'POST',
      `${lastWeightNumber}/delete`,
      token,
      dialogInputValue,
    );
    makeWeightRequest();
  };
  useEffect(() => {
    makeUserInfoRequest();
    makeWeightRequest();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <ProfilePageView
      name={firstName}
      weight={currentWeight}
      goalWeight={goalWeight}
      graphData={graphData}
      onWeightChange={addWeightRequest}
      isVisibleDiaolog={isVisibleDiaolog}
      setVisibleDiaolog={setVisibleDiaolog}
      dialogInputValue={dialogInputValue}
      setDialogInputValue={setDialogInputValue}
      onDialogButtonPress={addWeightRequest}
      onDeleteButtonPress={deleteWeightRequest}
      isLoaded={isLoaded}
    />
  );
};

export default ProfilePage;
