import React, {useState, useEffect} from 'react';
import SettingsPageView from './SettingsPageView';
import getUserInfoRequest from '../../../network/getUserInfoRequest';

import {useSelector} from 'react-redux';
import showSnackBar from '../../../components/showSnackbar';

const SettingsPage = () => {
  const token = useSelector(state => state.authentication.token);
  const [userInfo, setUserInfo] = useState(null);

  const {id, firstName, lastName, age, goalWeight, height} = userInfo
    ? userInfo
    : {
        id: 0,
        firstName: 'firstName',
        lastName: 'lastName',
        age: 0,
        goalWeight: 0,
        height: 0,
      };

  const makeUserInfoRequest = async () => {
    const {result, error} = await getUserInfoRequest(token);
    if (!error) setUserInfo(result);
    else {
      showSnackBar(error.message, 'SHORT', 'ОК', () => {});
    }
  };

  useEffect(() => {
    makeUserInfoRequest();
  });
  return (
    <SettingsPageView
      firstName={firstName}
      lastName={lastName}
      age={age}
      goalWeight={goalWeight}
      height={height}
      id={id}
      makeUserInfoRequest={makeUserInfoRequest}
    />
  );
};

export default SettingsPage;
