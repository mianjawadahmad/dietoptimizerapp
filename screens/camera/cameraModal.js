import {Modal, Text, View, StyleSheet} from "react-native";
import {Button, Icon, ListView, Title} from "@shoutem/ui";

const styles = StyleSheet.create({
    modalContainer: {
        backgroundColor: 'white',
        width: '80%',
        height: '70%',
        alignSelf: 'center',
        top: '15%',
        borderRadius: 3,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: {width: 100, height: 100},
        shadowOpacity: 0.8,
        shadowRadius: 82,
        elevation: 1,
    },
    modalRow: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        padding: 20,
    },
});

export default cameraModal = props => {
    const {title, children, isVisible, successBtn, closeBtn} = props;
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
            onRequestClose={() => console.log('close')}>
            <View
                style={styles.modalContainer}>
                <View
                    style={styles.modalRow}>
                    <View
                        style={{
                            width: '100%',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}>
                        <Title style={{marginBottom: 20}}>{title}</Title>
                        {children}
                    </View>
                    {successBtn}
                    {closeBtn}
                </View>
            </View>
        </Modal>
    );
}
