import React from 'react';
import {NativeRouter, Route, BackButton} from 'react-router-native';

import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import rootReducer from './redux/reducers';
import thunk from 'redux-thunk';

import Registration from './screens/authentication/registration';
import Login from './screens/authentication/login';
import MainPage from './screens/main-page';
import DietPage from './screens/diet-page';
import ProfilePage from './screens/profile-page';
import ProfileSettings from './screens/profile-page/settings-page';
import ProductForm from './screens/product-form';
import Camera from './screens/camera';
const store = createStore(rootReducer, applyMiddleware(thunk));
console.disableYellowBox = true;
const App = () => {
  console.disableYellowBox = true;

  return (
    <Provider store={store}>
      <NativeRouter>
        <BackButton>
          <Route exact path="/" component={Login} />
          <Route exact path="/registration" component={Registration} />
          <Route exact path="/main" component={MainPage} />
          <Route exact path="/diet" component={DietPage} />
          <Route exact path="/profile" component={ProfilePage} />
          <Route exact path="/product-form" component={ProductForm} />
          <Route exact path="/camera" component={Camera} />
          <Route exact path="/profile-settings" component={ProfileSettings} />
        </BackButton>
      </NativeRouter>
    </Provider>
  );
};

export default App;
