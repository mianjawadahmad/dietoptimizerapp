import authenticationRequest from '../../network/authenticationRequest';
import {startAuthentication, setAuthenticated, setError} from './dumbActions';
import showSnackBar from '../../components/showSnackbar';

export const authenticate = (login, password) => {
  return async dispatch => {
    dispatch(startAuthentication());
    const {result, error} = await authenticationRequest(login, password);
    if (error !== null) {
      dispatch(setError(error));
      showSnackBar(error.message, 'SHORT', 'ОК', () => {});
      return;
    }
    dispatch(setAuthenticated(login, password, result.token));
  };
};
