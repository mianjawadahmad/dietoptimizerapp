const mealsRequest = async token => {
  try {
    const response = await fetch(
      'http://tech.splinex-team.com:6667/api/v1/user/meal/get-all',
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: token,
        },
      },
    );

    if (!response.ok) {
      const error = await response.json();
      return {result: null, error};
    }

    const result = await response.json();
    return {result: result, error: null};
  } catch (error) {
    return {result: null, error};
  }
};

export default mealsRequest;
