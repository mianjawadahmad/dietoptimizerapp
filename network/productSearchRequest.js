const productSearchRequest = async (token, type, searchParams) => {
    try {
        const host = 'http://tech.splinex-team.com:6667/api/v1/user/product';
        const {search_key, search_value} = searchParams;
        const url = type === 'fulltext'
            ? `${host}/ft-search?q=${search_key}:${search_value}`
            : `${host}/search?q=${search_key}:${search_value}`;
        console.log('URL = ', url);
        const response = await fetch(
            `${url}`, {
                method: 'GET',
                headers: {
                  'Content-Type': 'application/json',
                    Authorization: token,
                },
            }
        );

        if (!response.ok) {
            const error = await response.json();
            return {result: null, error: error};
        }

        const result = await response.json();
        return {result: result, error: null};

    } catch (error) {
        return {result: null, error: error};
    }
};

export default productSearchRequest;
