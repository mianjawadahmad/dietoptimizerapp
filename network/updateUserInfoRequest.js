const updateUserInfoRequest = async (bodyContent, token) => {
  try {
    const response = await fetch(
      `http://tech.splinex-team.com:6667/api/v1/user/update-info`,
      {
        method: 'POST',
        body: JSON.stringify({
          ...bodyContent,
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer_${token}`,
        },
      },
    );

    if (!response.ok) {
      const error = await response.json();
      return {result: null, error};
    }

    const result = await response.json();
    return {result, error: null};
  } catch (error) {
    return {result: null, error};
  }
};

export default updateUserInfoRequest;
