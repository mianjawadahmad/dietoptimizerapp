import React from 'react';
import {StyleSheet} from 'react-native';
import {Screen, Spinner} from '@shoutem/ui';

const styles = StyleSheet.create({
  screen: {
    justifyContent: 'center',
  },
  spinner: {
    color: '#FF5722',
  },
});

const LoadingView = () => {
  return (
    <Screen style={styles.screen}>
      <Spinner style={styles.spinner} />
    </Screen>
  );
};

export default LoadingView;
