import React from 'react';
import {ActivityIndicator, StyleSheet} from 'react-native';
import {View, Text} from '@shoutem/ui';
import ProgressCircle from 'react-native-progress-circle';

const styles = StyleSheet.create({
  wrapper: {
    height: '100%',
    backgroundColor: '#FF5722',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    color: '#fff',
  },
  key: {
    fontSize: 12,
    color: '#ffffff',
  },
  value: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#ffffff',
  },
  col: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '33.33%',
  },
});

const colors = [
    'rgba(89, 196, 88, .7)',
    'rgba(191, 196, 73, .7)',
    'rgba(32, 35, 196, .7)',
    'rgba(196, 0, 31, .7)',
];

const CalorieProgressWidgetView = props => {
  const {percent, fat, carb, protein, isLoaded} = props;
    console.log('PERCENT = ', percent)
  const color = percent < 25
      ? colors[0]
      : ( percent < 50
          ? colors[1]
          : ( percent < 75
              ? colors[2] : colors[3]));
  if (!isLoaded) {
      return (
          <View style={styles.wrapper}>
              <ActivityIndicator style={{paddingTop: 15, color: 'white'}} color={'white'} size="large" />
          </View>
      );
  }
  return (
    <View style={styles.wrapper}>
      <ProgressCircle
        percent={percent}
        radius={80}
        outerCircleStyle={{marginTop: 10}}
        borderWidth={16}
        color={color}
        shadowColor="#FFD5C7"
        bgColor="#FF5722">
        <Text style={styles.text}>{`${percent}%`}</Text>
      </ProgressCircle>
      {percent > 70 &&
          <View style={{
              flexDirection: 'row',
              justifyContent: 'center',
              width: '100%',
              top: 0,
              backgroundColor: 'white',
              position: 'absolute',
          }}>
              <Text style={{color: 'red', fontSize: 18,}}>Вы близки к дневному лимиту</Text>
          </View>
      }

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          width: '100%',
          marginTop: 10,
        }}>
        <View style={styles.col}>
          <Text style={styles.value}>{protein || 0}</Text>
          <Text style={styles.key}>белки</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.value}>{fat || 0}</Text>
          <Text style={styles.key}>жиры</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.value}>{carb || 0}</Text>
          <Text style={styles.key}>углеводы</Text>
        </View>
      </View>
    </View>
  );
};
// chartConfig="{chartConfig}"
export default CalorieProgressWidgetView;
