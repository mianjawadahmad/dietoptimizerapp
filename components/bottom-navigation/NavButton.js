import React from 'react';
import {StyleSheet} from 'react-native';
import {Link} from 'react-router-native';
import {Button, Icon} from '@shoutem/ui';

const styles = StyleSheet.create({
  link: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
  button: {
    flex: 1,
  },
  icon: {
    color: '#A6A6A6',
  },
  iconActive: {
    color: '#FF5722',
  },
});

const NavButton = props => {
  const {to, iconName, isActive} = props;
  const iconStyle = isActive ? styles.iconActive : styles.icon;
  return (
    <Link style={styles.link} to={to} underlayColor="#E5E5E5">
      <Icon style={iconStyle} name={iconName} />
    </Link>
  );
};

export default NavButton;
